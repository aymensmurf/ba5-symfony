<?php

namespace App\Controller;

use App\Services\ApiCalls;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NewsListController extends AbstractController
{
    /**
     * @Route("/list", name="list")
     */
    public function index(ApiCalls $apiCalls): Response
    {
        $response = $apiCalls->getList();

        return $this->render('news_list/index.html.twig', [
            'list_news' => $response,
        ]);
    }
}
