<?php

namespace App\Controller;

use App\Form\DonorType;
use App\Services\ApiCalls;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DonorController extends AbstractController
{
    /**
     * @Route("/donor", name="donor")
     */
    public function index(ApiCalls $apiCalls, Request $request)
    {
        
        $form = $this->createForm(DonorType::class);

        
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $content = $form->getViewData();
            $data = $apiCalls->postDonor($content);
        }

        return $this->render('delivery/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
