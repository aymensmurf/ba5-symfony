<?php

namespace App\Services;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiCalls
{

    private $client;
    /**
     * 
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * 
     */
    public function getList(): array
    {
        $response = $this->client->request(
            'GET',
            'https://lebasproject.com/api/news',[
                'verify_peer' => false, 
                'verify_host' => false
            ]
            
        );
        // $contentType = 'application/json'
        $content = $response->getContent();
        // $content = '{"id":521583, "name":"symfony-docs", ...}'
        $content = $response->toArray();
        // $content = ['id' => 521583, 'name' => 'symfony-docs', ...]
        $data = $content['data']['data'];

        return $data;
    }

    /**
     * 
     */
    public function postDonor($content)
    {
        $response = $this->client->request(
            'POST',
            'https://lebas-uae.wh-international.net/api/donor/add',[
                'headers' => [
                    'Accept' => 'application/json',
                ],
                'verify_peer' => false,
                'body' => $content
            ]
        );
        // $contentType = 'application/json'
        $content = $response->toArray();

        return $content;
    }
}